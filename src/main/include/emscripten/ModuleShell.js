class ModuleShell {
  static includeFunction(moduleShell, prototype) {
    const moduleContext = moduleShell[this.MODULECONTEXT];
    let result = moduleContext.include(prototype);
    return result;
  }

  static includeMethod(moduleShell, method) {
    const moduleContext = moduleShell[this.MODULECONTEXT];
    const methods = moduleShell[this.METHODS];
    let key = method.name;
    methods[key] = methods[key] || method.bind(moduleContext);
    let value = methods[key];
    let result = [key, value];
    return result;
  }

  static include(moduleShell, source) {
    let result;
    if (source instanceof Function) {
      result = this.includeMethod(moduleShell, source);
    }
    else {
      result = this.includeFunction(moduleShell, source);
    }
    return result;
  }

  static bind(moduleShell, sources) {
    sources = Object.values(sources);
    let entries = sources.map(source => this.include(moduleShell, source));
    let bindings = Object.fromEntries(entries);
    let result = Object.assign(moduleShell, bindings);
    return result;
  }

  constructor(moduleContext, includes = []) {
    this[this.constructor.MODULECONTEXT] = moduleContext;
    this[this.constructor.METHODS] = {};
    this.constructor.bind(this, includes);
  }
}
ModuleShell.MODULECONTEXT = Symbol("WEBSPICE_MODULESHELL_MODULECONTEXT");
ModuleShell.METHODS = Symbol("WEBSPICE_MODULESHELL_METHODS");

export default ModuleShell;

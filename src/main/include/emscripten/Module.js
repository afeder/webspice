import Path from "../util/Path.js";

const defaultOptions = {
  "basePath": ""
};

class Module {
  constructor(options = Module.defaultOptions) {
    let selectedOptions = Object.assign({}, Module.defaultOptions, options);
    this[Module.OPTIONS] = selectedOptions;
  }

  locateFile(pathStr, prefix) {
    let basePathStr = this[Module.OPTIONS]["basePath"];
    let basePath = new Path(basePathStr);
    let resultPath = basePath.append(pathStr);
    let result = String(resultPath);
    return result;
  }
}
Module.OPTIONS = Symbol("WEBSPICE_MODULE_OPTIONS");
Module.defaultOptions = defaultOptions;

export default Module;

import FunctionDeclarator from "../../emscripten/declaration/FunctionDeclarator.js";
import SpiceInt from "../types/SpiceInt.js";
import SpiceChar from "../types/SpiceChar.js";
import SpiceBoolean from "../types/SpiceBoolean.js";

const bodc2n_c = new FunctionDeclarator("bodc2n_c", null);
bodc2n_c.addParameter("code", SpiceInt);
bodc2n_c.addParameter("lenout", SpiceInt);
bodc2n_c.addParameter("name", SpiceChar, true, undefined, true);
bodc2n_c.addParameter("found", SpiceBoolean, true, 1, true);

export {bodc2n_c};
export default bodc2n_c;

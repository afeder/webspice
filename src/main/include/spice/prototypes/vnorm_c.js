import FunctionDeclarator from "../../emscripten/declaration/FunctionDeclarator.js";
import SpiceDouble from "../types/SpiceDouble.js";
import ConstSpiceDouble from "../types/SpiceDouble.js";

const vnorm_c = new FunctionDeclarator("vnorm_c", SpiceDouble);
vnorm_c.addParameter("v1", ConstSpiceDouble, true, 3);

export {vnorm_c};
export default vnorm_c;

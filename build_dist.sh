#!/bin/sh
EMSCRIPTEN=$(em-config EMSCRIPTEN_ROOT)
BASE_DIR=$(pwd)
SRC_DIR=$BASE_DIR/src
LIB_DIR=$BASE_DIR/lib
BUILD_DIR=$BASE_DIR/build
DIST_DIR=$BASE_DIR/dist
COMPILER=$EMSCRIPTEN/emcc
NODE=$EMSDK_NODE
CC_INITIAL_MEMORY=$((65536*2000))
CC_EXPORTED_FUNCTIONS=$($NODE $SRC_DIR/settings.js --get EXPORTED_FUNCTIONS)
CC_EXPORTED_RUNTIME_METHODS=["FS"]
CC_O=1

echo "build_dist.sh: Building in $BUILD_DIR..."
BUILD_DIST_DIR=$BUILD_DIR/dist
BUILD_DIST_LIB_DIR=$BUILD_DIST_DIR/lib
mkdir -pv $BUILD_DIST_LIB_DIR
rm -rfv $BUILD_DIST_LIB_DIR/*
cd $BUILD_DIR
$COMPILER\
  -sINITIAL_MEMORY=$CC_INITIAL_MEMORY\
  -sEXPORTED_FUNCTIONS=$CC_EXPORTED_FUNCTIONS\
  -sEXPORTED_RUNTIME_METHODS=$CC_EXPORTED_RUNTIME_METHODS\
  -sMODULARIZE=1\
  -sEXPORT_ES6=1\
  -sWASM=1\
  --pre-js $SRC_DIR/pre.js\
  -O$CC_O\
  -o $BUILD_DIST_LIB_DIR/libwebspice.js\
  $LIB_DIR/libwebspice.a

echo "build_dist.sh: Transferring build to $DIST_DIR..."
mkdir -pv $DIST_DIR
rm -rfv $DIST_DIR/*
DIST_LIB_DIR=$DIST_DIR/include
mkdir -pv $DIST_LIB_DIR
cp -r $SRC_DIR/main/* $DIST_DIR
cp $BUILD_DIST_LIB_DIR/libwebspice.wasm $DIST_LIB_DIR/libwebspice.wasm
cp $BUILD_DIST_LIB_DIR/libwebspice.js $DIST_LIB_DIR/libwebspice.js

echo "build_dist.sh: Creating source code tarball in $BUILD_DIR..."
cd $BASE_DIR
tar -czvf $BUILD_DIR/webspice.tar.gz $(git ls-files)

echo "build_dist.sh: Saving build info to $DIST_DIR..."
$SRC_DIR/build/dist/gen_build_info.sh > $DIST_DIR/build_info.txt

echo "build_dist.sh: Creating npm package in $BUILD_DIR..."
npm pack --pack-destination $BUILD_DIR

echo "build_dist.sh: Done."

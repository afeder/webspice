import Demo from "./Demo.js";
import TargetStats from "./TargetStats.js";
import * as includes from "./includes.js";

const kernels = {
  "ephemeris": "../../../data/naif/generic_kernels/spk/planets/de432s.bsp",
  "leapseconds": "../../../data/naif/generic_kernels/lsk/latest_leapseconds.tls"
};

const PRECISION = 16;

const
  EARTH = 399,
  SUN = 10,
  MOON = 301,
  MERCURY = 199,
  VENUS = 299;

class Distances extends Demo {
  async initialize() {
    let shell = await super.initialize(includes);
    return shell;
  }

  async furnish(key) {
    const shell = await this.shell;
    const kernelPath = kernels[key];
    let result = shell.furnsh(kernelPath, (progress) => {
      this.onFileLoadProgress(kernelPath, progress);
    });
    return result;
  }

  async getBodyName(id) {
    const shell = await this.shell;
    let result = shell.bodc2n(id, 33);
    return result;
  }

  async getET(date = new Date()) {
    const shell = await this.shell;
    let strTime = this.constructor.getISOTime(date);
    let result = shell.str2et(strTime);
    return result;
  }

  async getTargetState(target = 0, observer = 0, et = 0) {
    const shell = await this.shell;
    let result = shell.spkez(target, et, "J2000", "NONE", observer);
    return result;
  }

  async getVectorNorm(vector) {
    const shell = await this.shell;
    let result = shell.vnorm(vector);
    return result;
  }

  layout() {
    const ui = this.ui;
    super.layout();

    let time = this.document.createElement("div");
    time.style.display = "none";
    ui.time = this.document.body.appendChild(time);
    let timeLabel = this.document.createElement("label");
    ui.timeLabel = ui.time.appendChild(timeLabel);
    ui.timeLabel.innerText = "Time (J2000): ";
    let timeValue = this.document.createElement("span");
    ui.timeValue = ui.time.appendChild(timeValue);

    ui.targets = [];
    ui.targets.push(new TargetStats(this, EARTH, SUN));
    ui.targets.push(new TargetStats(this, EARTH, MOON));
    ui.targets.push(new TargetStats(this, EARTH, MERCURY));
    ui.targets.push(new TargetStats(this, EARTH, VENUS));
    ui.targets.push(new TargetStats(this, SUN, EARTH));
    ui.targets.push(new TargetStats(this, SUN, MOON));
    ui.targets.push(new TargetStats(this, SUN, MERCURY));
    ui.targets.push(new TargetStats(this, SUN, VENUS));
  }

  async draw() {
    const ui = this.ui;
    super.draw();

    let et = await this.getET();
    let etFixed = et.toPrecision(PRECISION);
    ui.timeValue.innerText = `${etFixed}s`;

    await ui.targets.map((body) => body.draw(et));
  }

  async run() {
    const ui = this.ui;
    ui.fileLoaderLabel.innerText = "Loading kernels...";
    await this.furnish("ephemeris");
    await this.furnish("leapseconds");
    ui.time.style.display = "block";
    this.loop();
  }
}

export default Distances;

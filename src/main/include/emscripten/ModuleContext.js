import Invocation from "./Invocation.js";
import Heap from "./Heap.js";
import FileStore from "./FileStore.js";
import popIfInstanceOf from "../util/popIfInstanceOf.js";

class ModuleContext {
  constructor(libraryInstance) {
    this.instance = libraryInstance;
    this.functions = {};
  }

  get heap() {
    const moduleContext = this;
    const constructor = moduleContext.constructor;
    if (!this[constructor.HEAP]) {
      this[constructor.HEAP] = new constructor.Heap(moduleContext);
    }
    return this[constructor.HEAP];
  }

  get fileStore() {
    const moduleContext = this;
    const constructor = moduleContext.constructor;
    if (!this[constructor.FILESTORE]) {
      this[constructor.FILESTORE] = new constructor.FileStore(moduleContext);
    }
    return this[constructor.FILESTORE];
  }

  createInvocation(prototype, values = [], options = {}) {
    const moduleContext = this;
    const constructor = this.constructor;
    const type = options.invocationType || constructor.Invocation;
    let result = new type(moduleContext, prototype, values, options);
    return result;
  }

  createInvocable(prototype, options) {
    const moduleContext = this;
    let invocable = function (...values) {
      invocable.invocation = moduleContext.createInvocation(prototype, values, options);
      invocable.result = invocable.invocation.execute();
      invocable.outputs = invocable.invocation.outputs;
      return invocable.result;
    };
    invocable.cleanup = function () {
      if (invocable.invocation) {
        return invocable.invocation.cleanup();
      }
    };
    return invocable;
  }

  createWrap(prototype) {
    const moduleContext = this;
    let invoke = function (...args) {
      return moduleContext.invoke(prototype, ...args);
    };
    let invokeDirect = function (...values) {
      return invoke(values);
    }
    let result = invokeDirect;
    result.invoke = invoke;
    return result;
  }

  invoke(...args) {
    const moduleContext = this;
    let callback = popIfInstanceOf(args, Function);
    let [prototype, values, options] = args;

    let invocable = this.createInvocable(prototype, options);
    try {
      if (callback) {
        callback(invocable);
      }
      else {
        invocable(...values);
      }
    }
    finally {
      invocable.cleanup();
    }
    return invocable.result;
  }

  wrap(prototype) {
    const moduleContext = this;
    let key = prototype.identifier;
    let value = this.createWrap(prototype);
    let result = [key, value];
    return result;
  }

  include(prototype) {
    const moduleContext = this;
    const functions = moduleContext.functions;
    let entry = moduleContext.wrap(prototype);
    let key = entry[0];
    let value = entry[1];
    functions[key] = functions[key] || value;
    value = functions[key];
    let result = [key, value];
    return result;
  }

  import(prototype) {
    const moduleContext = this;
    let entry = this.include(prototype);
    let result = entry[1];
    return result;
  }
}
ModuleContext.HEAP = Symbol("WEBSPICE_MODULECONTEXT_HEAP");
ModuleContext.FILESTORE = Symbol("WEBSPICE_MODULECONTEXT_FILESTORE");
ModuleContext.Invocation = Invocation;
ModuleContext.Heap = Heap;
ModuleContext.FileStore = FileStore;

export default ModuleContext;

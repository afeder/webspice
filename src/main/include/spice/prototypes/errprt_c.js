import FunctionDeclarator from "../../emscripten/declaration/FunctionDeclarator.js";
import ConstSpiceChar from "../types/ConstSpiceChar.js";
import SpiceInt from "../types/SpiceInt.js";
import SpiceChar from "../types/SpiceChar.js";

const errprt_c = new FunctionDeclarator("errprt_c", null);
errprt_c.addParameter("op", ConstSpiceChar, true);
errprt_c.addParameter("lenout", SpiceInt);
errprt_c.addParameter("list", SpiceChar, true, undefined, true);

export {errprt_c};
export default errprt_c;

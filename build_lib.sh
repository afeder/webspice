#!/bin/sh
EMSCRIPTEN=$(em-config EMSCRIPTEN_ROOT)
BASE_DIR=$(pwd)
SRC_DIR=$BASE_DIR/upstream/import/cspice/src/cspice
SRC_BUILD_DIR=$BASE_DIR/src/build/lib/cspice
BUILD_DIR=$BASE_DIR/build/lib/cspice
LIB_DIR=$BASE_DIR/lib
ARCHIVER=$EMSCRIPTEN/emar
mkdir -pv $BUILD_DIR
rm -rfv $BUILD_DIR/*
mkdir -pv $LIB_DIR
rm -rfv $LIB_DIR/*
cd $BUILD_DIR
cp -r $SRC_DIR/* $BUILD_DIR
cp -r $SRC_BUILD_DIR/* $BUILD_DIR
./customize.sh
./mkprodct.csh
$ARCHIVER rcsv $LIB_DIR/libwebspice.a $BUILD_DIR/*.o
echo "build_lib.sh: Done."

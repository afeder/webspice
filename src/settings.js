const EXPORTED_FUNCTIONS = [
  "_malloc",
  "_free",
  "_tkvrsn_c",
  "_erract_c",
  "_errprt_c",
  "_failed_c",
  "_getmsg_c",
  "_furnsh_c",
  "_bodc2n_c",
  "_str2et_c",
  "_spkez_c",
  "_vnorm_c"
];

const settings = {
  EXPORTED_FUNCTIONS
};

const thisModule = module || this.module;
if (thisModule instanceof Object) {
  const thisProcess = process || this.process;
  if (thisProcess instanceof Object) {
    if (thisProcess.argv) {
      if (thisProcess.argv[1]) {
        if (thisProcess.argv[1] === thisModule.filename) {
          if (thisProcess.argv[2]) {
            if (thisProcess.argv[2] === "--get") {
              let key = process.argv[3];
              let value = settings[key];
              if (value === undefined) {
                throw new RangeError("Invalid key.");
              }
              let output = JSON.stringify(value);
              console.log(output);
            }
          }
        }
      }
    }
  }
  else if (thisModule.exports instanceof Object) {
    Object.assign(thisModule.exports, settings);
  }
}

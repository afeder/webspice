class Double extends Float64Array {
  valueOf() {
    let result = Array.from(this);
    return result;
  }
}
Double.pointer = false;
Double.cardinality = 1;

export default Double;

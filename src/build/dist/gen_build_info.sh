#!/bin/sh
# This script collects and outputs build info. It is meant be run from the root
# of the repo.
BASE_DIR=$(pwd)
BUILD_DIR=$BASE_DIR/build
NODE=$EMSDK_NODE

# Collect build info.
BUILD_TIME_ISO=$(date -u -Iseconds)
BUILD_TIME_UNIX=$(date +%s%N)
BUILD_HASH=$(sha1sum < $BUILD_DIR/webspice.tar.gz | cut -d ' ' -f 1)
WEBSPICE_VER=$($NODE -e "const fs=require('fs'); input = fs.readFileSync(0).toString(); const package_info = JSON.parse(input); console.log(package_info.name, package_info.version);" < $BASE_DIR/package.json)
WEBSPICE_COMMIT=$(git log --pretty=format:'%H' -n 1)
EMSCRIPTEN_VER=$(emcc -v 2>&1 >/dev/null | head -n 4)

# Output build info.
# The line ordering represents a data format. The variables are quoted to
# preserve any newlines in them.
echo "$BUILD_TIME_ISO"
echo "$BUILD_TIME_UNIX"
echo "$BUILD_HASH"
echo "$WEBSPICE_VER ($WEBSPICE_COMMIT)"
echo "$EMSCRIPTEN_VER"

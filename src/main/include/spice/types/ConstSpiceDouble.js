import SpiceDouble from "./SpiceDouble.js";
import Const from "../../emscripten/declaration/Const.js";

// upstream/import/cspice/include/SpiceZdf.h
export class ConstSpiceDouble extends SpiceDouble {
}
Const.lock(ConstSpiceDouble)

export default ConstSpiceDouble;

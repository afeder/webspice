import App from "./App.js";

class Demo extends App {
  constructor(window, document) {
    super(window, document);
    this.ui = {};
    this.layout();
  }

  onFileLoadProgress(source, progress) {
    const ui = this.ui;
    if (!progress.done) {
      ui.fileLoaderLabel.innerText = `Loading ${source}...`;
      ui.fileLoaderProgress.style.display = "block";
      ui.fileLoader.style.display = "block";
    }
    else {
      ui.fileLoaderLabel.innerText = `Loaded ${source}.`;
      ui.fileLoaderProgress.style.display = "none";
      ui.fileLoader.style.display = "none";
    }
    let progressFraction = progress.receivedLength/progress.contentLength;
    let progressValue = progressFraction*100;
    ui.fileLoaderProgress.value = progressValue;
    ui.fileLoaderProgress.innerText = `${progressValue}%`;
  }

  async loadFile(path) {
    let file = await super.loadFile(path, (resource, progress) => {
      const source = resource.source;
      onFileLoadProgress(source, progress);
    });
    return file;
  }

  layout() {
    const ui = this.ui;

    let fileLoader = this.document.createElement("div");
    ui.fileLoader = this.document.body.appendChild(fileLoader);
    let fileLoaderLabel = this.document.createElement("label");
    fileLoaderLabel.htmlFor = "fileLoaderProgress";
    ui.fileLoaderLabel = ui.fileLoader.appendChild(fileLoaderLabel);
    let fileLoaderProgress = this.document.createElement("progress");
    fileLoaderProgress.id = "fileLoaderProgress";
    fileLoaderProgress.max = 100;
    fileLoaderProgress.value = 0;
    fileLoaderProgress.style.display = "none";
    ui.fileLoaderProgress = ui.fileLoader.appendChild(fileLoaderProgress);

    let statistics = this.document.createElement("div");
    statistics.style.opacity = 0.125;
    statistics.style.position = "absolute";
    statistics.style.right = "8px";
    statistics.style.bottom = "8px";
    ui.statistics = this.document.body.appendChild(statistics);
    let statisticsMemory = this.document.createElement("span");
    ui.statisticsMemory = ui.statistics.appendChild(statisticsMemory);
    ui.statistics.appendChild(this.document.createTextNode(" "));
    let statisticsFPS = this.document.createElement("span");
    ui.statisticsFPS = ui.statistics.appendChild(statisticsFPS);
  }

  async draw() {
    const heap = await this.heap;
    const ui = this.ui;

    let bytesUsed = Number(heap.statistics.bytesUsed);
    let bytesTotal = Number(heap.byteLength);
    ui.statisticsMemory.innerText = `${bytesUsed}B/${bytesTotal}B`;
    let fps = this.statistics.FPS.toFixed(4);
    ui.statisticsFPS.innerText = `${fps}FPS`;
    ui.statisticsMemory.style.color = bytesUsed ? "red" : "unset";
    ui.statistics.style.opacity = bytesUsed ? 1 : 0.125;
  }

  loop() {
    let t0 = this.statistics.lastFrameTimestamp || 0;
    let t1 = this.statistics.lastFrameTimestamp = performance.now();
    let frameTime = this.statistics.frameTime = t1 - t0;
    this.statistics.FPS = 1/(frameTime/1000);
    this.draw();
    this.window.setTimeout(() => this.loop());
  }
}

export default Demo;

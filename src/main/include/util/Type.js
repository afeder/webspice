class Type {
  static isInteger(value) {
    let result = Number.isInteger(value);
    return result;
  }

  static isNumber(value) {
    let result = value === Number(value);
    return result;
  }

  static isArray(value) {
    let result = Array.isArray(value);
    return result;
  }

  static isString(value) {
    let result = typeof value === "string";
    return result;
  }
}

export default Type;

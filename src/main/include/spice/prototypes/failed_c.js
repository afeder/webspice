import FunctionDeclarator from "../../emscripten/declaration/FunctionDeclarator.js";
import SpiceBoolean from "../types/SpiceBoolean.js";

const failed_c = new FunctionDeclarator("failed_c", SpiceBoolean);

export {failed_c};
export default failed_c;

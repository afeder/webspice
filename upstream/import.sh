#!/bin/sh
BASE_DIR=$(pwd)
DOWNLOAD_DIR=$BASE_DIR/download
IMPORT_DIR=$BASE_DIR/import
mkdir -pv $IMPORT_DIR
rm -rfv $IMPORT_DIR/*
cd $IMPORT_DIR
cp $DOWNLOAD_DIR/* $IMPORT_DIR
/bin/csh -f importCSpice.csh
echo "import.sh: Done."

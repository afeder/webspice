import SpiceChar from "../spice/types/SpiceChar.js";
import SpiceBoolean from "../spice/types/SpiceBoolean.js";
import bodc2n_c from "../spice/prototypes/bodc2n_c.js";

function bodc2n(code, lenout=256) {
  const webspice = this.webspice;
  const functions = webspice.functions;
  const heap = webspice.heap;
  webspice.include(bodc2n_c);
  let name = heap.allocate(new SpiceChar(lenout));
  let found = heap.allocate(new SpiceBoolean(1));
  functions.bodc2n_c(code, lenout, name, found);
  let result = name.toString();
  name.free()
  found.free()
  return result;
}

export {bodc2n};
export default bodc2n;

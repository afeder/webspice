#!/bin/sh
# This script customizes CSPICE source code files for our build. It is intended
# to be run in a directory with the CSPICE source code files.

# Replace all declarations of "int s_copy" with "void s_copy" (see #1).
sed -i 's/int s_copy/void s_copy/g' *.c

# Replace all declarations of "int zzsetnnread_" with "void zzsetnnread_" (see #1).
sed -i 's/int zzsetnnread_/void zzsetnnread_/g' *.c

# Replace all declarations of "int s_cat" with "void s_cat" (see #1).
sed -i 's/int s_cat/void s_cat/g' *.c

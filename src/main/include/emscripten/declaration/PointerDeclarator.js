import Declarator from "./Declarator.js";
import Pointer from "../types/Pointer.js";

class PointerDeclarator extends Declarator {
  constructor(identifier, pointer = false, cardinality = 1) {
    super(identifier, Pointer, pointer, cardinality);
  }
}

export default PointerDeclarator;

import str2et_c from "../spice/prototypes/str2et_c.js";

function str2et(time) {
  const webspice = this.webspice;
  const functions = webspice.functions;
  webspice.include(str2et_c);
  let result;
  functions.str2et_c.invoke(function (execute) {
    execute(time);
    result = execute.outputs.et[0];
  });
  return result;
}

export {str2et};
export default str2et;

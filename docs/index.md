## How it works

### CSPICE
The core functionality of webspice - the calculation of astronomical
observation geometry - comes from NASA's [CSPICE](https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/cspice.html#Abstract) toolkit. CSPICE is based on functions originally
written in the Fortran programming language, and is part of a greater family of
tools called [SPICE](https://naif.jpl.nasa.gov/naif/toolkit.html).

The CSPICE toolkit is an industry standard and is used for astronomy and space
exploration planning across the world. The most central of the toolkit's many
features is its ability to interpolate the position and other parameters of
objects in space, at arbitrary points in time, from a pre-generated model called
an *ephemeris*. In short, an ephemeris is a pre-calculated table that lists the
position of a given astronomical object at a (usually large) number of points
in time, which can then be used as the basis for interpolating the object's
position at other times. Such tables, or *ephemerides*, of scientific quality,
are available from NASA in the form of so-called *kernels*, which are special
data files in a format that can be processed by CSPICE. A small selection of
kernels can be found in the [`data`](/data) directory of the repository.

CSPICE is written in the C programming language and its source code is freely
available from NASA's website. A copy of this code is located under the
[`upstream/import`](/upstream/import) directory of the repository. The included
copy of CSPICE can be updated with the two scripts in the [`upstream`](/upstream)
directory: First the latest CSPICE package from NAIF is downloaded with the
[`upstream/download.sh`](/upstream/download.sh) script. Then the downloaded
package is imported (unpackaged) with the [`upstream/import.sh`](/upstream/import.sh)
script.

### Build scripts
Since JavaScript code can not be directly linked against code written in
languages such as C, CSPICE can not be immediately used from environments that
only support JavaScript, like web browsers. To be able to utilize the functions
of CSPICE from JavaScript, webspice therefore is built by first compiling the
CSPICE functions into [WebAssembly](https://webassembly.org/) - a binary code
format that can be directly executed by modern JavaScript interpreters. This
compilation is done in two steps using the [Emscripten](https://emscripten.org/docs/compiling/WebAssembly.html) toolchain.

The first step is performed by the [`build_lib.sh`](/build_lib.sh) script
located in the root of the repository. Using Emscripten, the `build_lib.sh`
script first compiles all the relevant CSPICE source files into [LLVM IR](https://llvm.org/docs/LangRef.html#abstract) object files, and then packages these object files into a
single static library file called [`libwebspice.a`](/lib/libwebspice.a) in the
`lib` directory of the repository.

The second step is performed by the [`build_dist.sh`](/build_dist.sh) script,
also located in the root of the repository. Using Emscripten, the
`build_dist.sh` script compiles a subset of functions in the static library file
into WebAssembly. This produces a binary file containing the actual WebAssembly
bytecode, called `libwebspice.wasm`, and a JavaScript file, called
`libwebspice.js`, used to load the WebAssembly module, with the aid of certain
low-level glue functionality provided by Emscripten. Both of these files are
copied by the script into the [`dist`](/dist) directory of the repository,
together with the main webspice JavaScript library (see below).

### JavaScript library
Because the C language is a comparatively lower-level language than JavaScript,
the raw interface of the CSPICE binary produced by Emscripten lacks many of the
conveniences that a typical JavaScript programmer would demand. For instance,
the raw module returns string values as numerical memory addresses rather than
as the familiar string objects that JavaScript programmers are used to.

To this end, webspice includes a layer of JavaScript code that seeks to abstract
some of the idiosyncracies of the low-level WebAssembly interface of the CSPICE
module into more useful, idiomatic JavaScript functions. This interface is the
recommended interface for most users. It is implemented in the [`src/main/include`](/src/main/include) directory of the repository.

At the lowest level of this JavaScript library are the classes in the [`src/main/include/emscripten`](/src/main/include/emscripten) directory. These classes provide
general methods for interacting with modules produced by Emscripten, with as few
webspice-specific implementation details as possible. This includes methods for
invoking functions in a module and for accessing memory and virtual file
systems managed by the module.

At the next level are the classes and objects in the [`src/main/include/spice`](/src/main/include/spice) directory. They define CSPICE-specific bindings for functions and data
types of the CSPICE WebAssembly module. They are made for use against the
general methods described above.

At the next level again are the functions in the [`src/main/include/methods`](/src/main/include/methods) directory. These are high-level, idiomatic functions over the
underlying CSPICE functions. They rely on the classes and bindings described
above internally, but take input arguments and return output values in a way
that a JavaScript programmer will feel accustomed to.

Finally, at the highest level are the classes in root of the [`src/main/include`](/src/main/include) directory itself. These classes tie the levels described above together to
present a convenient, webspice-specific interface that can be easily imported
into and invoked by other JavaScript projects to calculate astronomical
observation geometry of scientific quality.

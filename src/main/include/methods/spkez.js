import SpiceDouble from "../spice/types/SpiceDouble.js";
import spkez_c from "../spice/prototypes/spkez_c.js";

function spkez(targ, et, ref, abcorr, obs) {
  const webspice = this.webspice;
  const functions = webspice.functions;
  const heap = webspice.heap;
  webspice.include(spkez_c);
  let starg = new SpiceDouble(6);
  starg = heap.allocate(starg);
  let lt = new SpiceDouble(1);
  lt = heap.allocate(lt);
  functions.spkez_c(targ, et, ref, abcorr, obs, starg, lt);
  let result = [[...starg], lt[0]];
  starg.free();
  lt.free();
  return result;
}

export {spkez};
export default spkez;

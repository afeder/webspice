import SpiceChar from "./SpiceChar.js";
import Const from "../../emscripten/declaration/Const.js";

// upstream/import/cspice/include/SpiceZdf.h
export class ConstSpiceChar extends SpiceChar {
}
Const.lock(ConstSpiceChar)

export default ConstSpiceChar;

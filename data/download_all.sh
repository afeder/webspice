#!/bin/sh
BASE_DIR="$(pwd)"
DOWNLOAD_DIR="$BASE_DIR/naif"
URL="ftp://naif.jpl.nasa.gov/pub/naif/"
WGET_URL="$URL;type=i"
WGET_CUTDIRS=2
WGET_EXCLUDE="cosmographia/,deprecated_kernels/,misc/,self_training/,toolkit/,toolkit_docs/,utilities/"
DOWNLOAD_CMD="wget -m -N -nH --cut-dirs=$WGET_CUTDIRS -X $WGET_EXCLUDE $WGET_URL"
echo "This program downloads a full mirror of the NASA/NAIF FTP data site to"
echo "the current working directory from:"
echo
echo "  $URL"
echo
echo "The operation may take multiple weeks on a standard internet connection"
echo "and require several terabytes of free disk space."
echo "Press Enter to continue."
read _
mkdir -pv "$DOWNLOAD_DIR"
cd "$DOWNLOAD_DIR"
$DOWNLOAD_CMD

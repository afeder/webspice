import File from "./File.js";

const defaultOptions = {
  "fetch": fetch
}

class FileStore {
  constructor(moduleContext, options = FileStore.defaultOptions) {
    this.moduleContext = moduleContext;
    this.options = options;
    this.index = new Array();
  }

  get fileSystem() {
    let moduleContext = this.moduleContext;
    let instance = moduleContext.instance;
    let result = instance.FS;
    return result;
  }

  create() {
    let fileSystem = this.fileSystem;
    let file = new File(fileSystem);
    let id = this.index.push(file);
    let path = String(id);
    file.path = path;
    return file;
  }

  async persist(input, onProgress) {
    let data = await File.makeData(input, onProgress);
    let file = this.create();
    file.writeFile(data);
    return file;
  }
}
FileStore.defaultOptions = defaultOptions;

export default FileStore;

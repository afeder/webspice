import FunctionDeclarator from "../../emscripten/declaration/FunctionDeclarator.js";
import ConstSpiceChar from "../types/ConstSpiceChar.js";
import SpiceInt from "../types/SpiceInt.js";
import SpiceChar from "../types/SpiceChar.js";

const getmsg_c = new FunctionDeclarator("getmsg_c", null);
getmsg_c.addParameter("option", ConstSpiceChar, true);
getmsg_c.addParameter("lenout", SpiceInt);
getmsg_c.addParameter("msg", SpiceChar, true, undefined, true);

export {getmsg_c};
export default getmsg_c;

function popIfInstanceOf(array, type) {
  let lastIndex = array.length - 1;
  let lastArgument = array[lastIndex];
  let result;
  if (lastArgument instanceof type) {
    result = array.pop();
  }
  return result;
}

export default popIfInstanceOf;

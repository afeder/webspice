class Float extends Float32Array {
  valueOf() {
    let result = Array.from(this);
    return result;
  }
}
Float.pointer = false;
Float.cardinality = 1;

export default Float;

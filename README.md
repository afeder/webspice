# webspice

webspice is an unofficial, experimental port of NASA's
[SPICE](https://naif.jpl.nasa.gov/naif/spiceconcept.html) toolkit for
astronomical observation geometry to [WebAssembly](https://webassembly.org/).
The aim is to enable on-line, interactive computations of advanced space
observation geometry in [any modern web
browser](https://webassembly.org/roadmap/) without expensive server-side
infrastructure or unreliable proprietary browser plugins.

The port is based on NASA's official
[CSPICE](https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/req/cspice.html#Abstract)
implementation in C, compiled to WebAssembly using
[Emscripten](https://emscripten.org/docs/compiling/WebAssembly.html).

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install the webspice
package as a local dependency under your current working directory.

```bash
npm install webspice
```

## Usage

webspice takes the form of a JavaScript module. To use the module in your own
JavaScript code, import it using the
[`import`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)
statement:

```javascript
import webspice from "./node_modules/webspice/index.js";
```

The location specifier must point to the location of the module in your
application. Provided the browser is able to load the module from the specified
location, it will bind the module's default export to the `webspice` identifier.

Note that the
[`import`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)
statement may only be used from scripts that are themselves marked as
`type="module"`; refer to the JavaScript documentation for details.

To instantiate the module, invoke the `instantiate` static method:

```javascript
let instantiateWebspice = webspice.instantiate();
```

The method returns a JavaScript promise that is fulfilled once the module
instance has been created. By default, the module seeks to load the WebAssembly
library from the path where the module was imported from.

If the promise resolves succesfully, it will pass an instance that you can use
to invoke the functions of the library:

```javascript
instantiateWebspice.then(
  function (instance) {
    console.log(instance.tkvrsn_c("TOOLKIT"));
  }
);
```

## Building

Building the webspice package from source comprise two separate parts: the
CSPICE static library and the webspice library itself.

Both parts are built using Emscripten, and the path of your Emscripten toolchain must be
in your shell search path for the build scripts to work. Refer to the [Emscripten
documentation](https://emscripten.org/docs/tools_reference/emsdk.html#emsdk-set-active-tools)
for more information on how to accomplish this using `emsdk_env.sh`.

### Updating CSPICE
The webspice repo comes with a copy of the official CSPICE toolkit and source
code. These files can be found under [`upstream/import`](upstream/import). If this copy is
sufficient for you, you can skip to the next section. Should you wish to update
the copy of CSPICE from the NASA/NAIF website, however, this can be accomplished
by using the two scripts in the [`upstream`](upstream) directory.

To download the latest CSPICE package from NAIF into the [`upstream/download`](upstream/download)
directory, from the [`upstream`](upstream) directory, run:

```bash
./download.sh
```

To import (unpack) the downloaded package into the [`upstream/import`](upstream/import)
directory, again from the [`upstream`](upstream) directory, run:

```bash
./import.sh
```

### Building CSPICE
The official CSPICE toolkit contains scripts for compiling the CSPICE source
code to native binaries, but these scripts are not used for webspice. Instead, a
select subset of the CSPICE source code is compiled to a static LLVM IR library
at [`lib/libwebspice.a`](lib/libwebspice.a) with Emscripten. To build the
library, from the top-level directory, run:

```bash
./build_lib.sh
```

The path of your Emscripten toolchain must be in your shell search path for the
build script to work.

### Building webspice
To build the webspice library itself into the [`dist`](dist) directory, from the
top-level directory, run:

```bash
./build_dist.sh
```

The path of your Emscripten toolchain must be in your shell search path for the
build script to work, and the CSPICE static library must have already been built
to [`lib/libwebspice.a`](lib/libwebspice.a) (see previous section).

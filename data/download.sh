#!/bin/sh
BASE_DIR="$(pwd)"
DOWNLOAD_DIR="$BASE_DIR/naif"
URL="ftp://naif.jpl.nasa.gov/pub/naif/generic_kernels/"
WGET_URL="$URL;type=i"
WGET_CUTDIRS=2
DOWNLOAD_CMD="wget -m -N -nH --cut-dirs=$WGET_CUTDIRS $WGET_URL"
mkdir -pv "$DOWNLOAD_DIR"
cd "$DOWNLOAD_DIR"
$DOWNLOAD_CMD

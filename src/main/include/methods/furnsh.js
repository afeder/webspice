import Type from "../util/Type.js";
import Resource from "../web/Resource.js";
import furnsh_c from "../spice/prototypes/furnsh_c.js";

async function furnsh(input, onProgress) {
  const webspice = this.webspice;
  const functions = webspice.functions;
  const fileStore = webspice.fileStore;
  if (Type.isString(input)) {
    input = new Resource(input);
  }
  webspice.include(furnsh_c);
  let file = await fileStore.persist(input, onProgress);
  functions.furnsh_c(file.path);
}

export {furnsh};
export default furnsh;

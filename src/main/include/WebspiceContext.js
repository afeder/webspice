import ModuleContext from "./emscripten/ModuleContext.js";
import WebspiceModule from "./WebspiceModule.js";
import SpiceChar from "./spice/types/SpiceChar.js";
import erract_c from "./spice/prototypes/erract_c.js";
import errprt_c from "./spice/prototypes/errprt_c.js";
import failed_c from "./spice/prototypes/failed_c.js";
import getmsg_c from "./spice/prototypes/getmsg_c.js";

const webspiceObjectProperties = {
  "invoke": {
    value: function (...args) {
      const context = this;
      const heap = context.heap;
      const functions = context.functions;
      functions.erract_c("SET", 0, "RETURN");
      functions.errprt_c("SET", 0, "NONE");
      let result = context.constructor.prototype.invoke.apply(this, args);
      let failed = functions.failed_c();
      if (failed[0] === failed.constructor.SPICETRUE) {
        let msg = heap.allocate(new SpiceChar(2048));
        functions.getmsg_c("SHORT", msg.byteLength, msg);
        let msgShort = msg.toString();
        functions.getmsg_c("EXPLAIN", msg.byteLength, msg);
        let msgExplain = msg.toString();
        functions.getmsg_c("LONG", msg.byteLength, msg);
        let msgLong = msg.toString();
        msg.free();
        let errorTitle = `${msgShort} ${msgExplain}`.trim();
        let errorMessage = [errorTitle, msgLong].join(": ");
        let error = new Error(errorMessage);
        error.title = errorTitle;
        error.msgShort = msgShort;
        error.msgExplain = msgExplain;
        error.msgLong = msgLong;
        throw error;
      }
      return result;
    }
  }
}

class WebspiceContext extends ModuleContext {
  static async instantiate(options) {
    let libraryInstance = await WebspiceModule.instantiate(options);
    let webspiceContext = new WebspiceContext(libraryInstance);
    return webspiceContext;
  }

  constructor(...args) {
    super(...args);
    this.include(erract_c);
    this.include(errprt_c);
    this.include(failed_c);
    this.include(getmsg_c);
    this.webspice = Object.create(this, webspiceObjectProperties);
  }
}

export default WebspiceContext;

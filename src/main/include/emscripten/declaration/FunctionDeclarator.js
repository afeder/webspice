import Declarator from "./Declarator.js";
import ParameterDeclarator from "./ParameterDeclarator.js";

class FunctionDeclarator extends Declarator {
  constructor(identifier, returnType, returnPointer, returnCardinality) {
    super(identifier, returnType, returnPointer, returnCardinality)
    this.parameters = [];
  }

  addParameter(...args) {
    let parameterDeclarator = new ParameterDeclarator(...args);
    this.parameters.push(parameterDeclarator);
    return parameterDeclarator;
  }
}

export default FunctionDeclarator;

import FunctionDeclarator from "../../emscripten/declaration/FunctionDeclarator.js";
import ConstSpiceChar from "../types/ConstSpiceChar.js";
import SpiceDouble from "../types/SpiceDouble.js";

const str2et_c = new FunctionDeclarator("str2et_c", null);
str2et_c.addParameter("str", ConstSpiceChar, true);
str2et_c.addParameter("et", SpiceDouble, true, 1, true);

export {str2et_c};
export default str2et_c;

import FunctionDeclarator from "../../emscripten/declaration/FunctionDeclarator.js";
import SpiceInt from "../types/SpiceInt.js";
import SpiceDouble from "../types/SpiceDouble.js";
import ConstSpiceChar from "../types/ConstSpiceChar.js";

const spkez_c = new FunctionDeclarator("spkez_c", null);
spkez_c.addParameter("targ", SpiceInt);
spkez_c.addParameter("et", SpiceDouble);
spkez_c.addParameter("ref", ConstSpiceChar, true);
spkez_c.addParameter("abcorr", ConstSpiceChar, true);
spkez_c.addParameter("obs", SpiceInt);
spkez_c.addParameter("starg", SpiceDouble, true, 6, true);
spkez_c.addParameter("lt", SpiceDouble, true, 1, true);

export {spkez_c};
export default spkez_c;

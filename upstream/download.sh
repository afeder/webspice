#!/bin/sh
BASE_DIR=$(pwd)
DOWNLOAD_DIR=$BASE_DIR/download
IMPORT_DIR=$BASE_DIR/import
mkdir -pv $DOWNLOAD_DIR
rm -rfv $DOWNLOAD_DIR/*
cd $DOWNLOAD_DIR
wget http://naif.jpl.nasa.gov/pub/naif/toolkit//C/PC_Linux_GCC_64bit/packages/cspice.tar.Z
wget http://naif.jpl.nasa.gov/pub/naif/toolkit//C/PC_Linux_GCC_64bit/packages/importCSpice.csh
echo "download.sh: Done."

class Delimiter {
  constructor(value = ",") {
    let sanitizedValue = String(value);
    this.value = sanitizedValue;
  }

  split(value = "") {
    let sanitizedValue = String(value);
    let delimiter = this.value;
    let result = sanitizedValue.split(delimiter);
    return result;
  }

  join(value = []) {
    let delimiter = this.value;
    let result = value.join(delimiter);
    return result;
  }

  toString() {
    let result = String(this.value);
    return result;
  }
}

export default Delimiter;

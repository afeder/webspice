import Type from "../util/Type.js";
import PointerDeclarator from "./declaration/PointerDeclarator.js";

class WrongCardinalityError extends TypeError {
  constructor(identifier) {
    let message = `Wrong cardinality for "${identifier}".`;
    super(message);
  }
}

class Invocation {
  static coerce(declarator, value) {
    if (declarator.type === null) {
      return null;
    }
    if (value === undefined) {
      value = new declarator.type(declarator.cardinality);
    }
    if (value.length === undefined) {
      value = [value];
    }
    if (declarator.cardinality && value.length !== declarator.cardinality) {
      throw new WrongCardinalityError(declarator.identifier);
    }
    if (!(value instanceof declarator.type)) {
      value = new declarator.type(value);
    }
    return value;
  }

  static coercePointer(value) {
    const declarator = new PointerDeclarator();
    let result = this.coerce(declarator, value);
    return result;
  }

  static isCollectible(allocatedValue) {
    let result = allocatedValue.retain !== true;
    return result;
  }

  static onParameterValues(values) {
  }

  static onDeallocating(allocatedValue) {
  }

  constructor(moduleContext, prototype, values = [], options = {}) {
    this.moduleContext = moduleContext;
    this.prototype = prototype;
    this.values = values;
    this.options = options;
    this.allocatedValues = [];
  }

  get instance() {
    const moduleContext = this.moduleContext;
    let result = moduleContext.instance;
    return result;
  }

  get heap() {
    const moduleContext = this.moduleContext;
    let result = moduleContext.heap;
    return result;
  }

  get mangledIdentifier() {
    const prototype = this.prototype;
    let result = "_" + prototype.identifier;
    return result;
  }

  get targetFunction() {
    const instance = this.instance;
    const mangledIdentifier = this.mangledIdentifier;
    let result = instance[mangledIdentifier];
    return result;
  }

  onParameterValues(...args) {
    const options = this.options;
    const onParameterValues =
      options.onParameterValues || this.constructor.onParameterValues;
    let result = onParameterValues.apply(this, args);
    return result;
  }

  onDeallocating(...args) {
    const options = this.options;
    const onDeallocating = options.onDeallocating || this.constructor.onDeallocating;
    let result = onDeallocating.apply(this, args);
    return result;
  }

  allocate(value) {
    const heap = this.heap;
    let allocation = heap.allocate(value);
    return allocation;
  }

  free(allocation) {
    const heap = this.heap;
    let result = heap.free(allocation);
    return result;
  }

  allocateDeclaredValue(declarator, value) {
    let allocatedValue = this.allocate(value);
    allocatedValue.declarator = declarator;
    allocatedValue.retain = false;
    this.allocatedValues.push(allocatedValue);
    return allocatedValue;
  }

  cleanup() {
    const allocatedValues = this.allocatedValues;
    allocatedValues.forEach(this.onDeallocating, this);
    let collectibleAllocations =
      allocatedValues.filter(this.constructor.isCollectible, this);
    let result = collectibleAllocations.map(this.free, this);
    return result;
  }

  toByteOffset(declarator, typedValue) {
    const heap = this.heap;
    if (!heap.containsView(typedValue)) {
      typedValue = this.allocateDeclaredValue(declarator, typedValue);
    }
    let byteOffset = typedValue.byteOffset;
    return byteOffset;
  }

  fromByteOffset(declarator, byteOffset) {
    const heap = this.heap;
    const cardinality = declarator.cardinality;
    let length = Type.isInteger(cardinality) ? cardinality : undefined;
    let typedValue = new declarator.type(heap.buffer, byteOffset, length);
    return typedValue;
  }

  toPointer(declarator, typedValue) {
    let byteOffset = this.toByteOffset(declarator, typedValue);
    let pointer = [byteOffset];
    pointer = this.constructor.coercePointer(pointer);
    return pointer;
  }

  fromPointer(declarator, pointer) {
    pointer = this.constructor.coercePointer(pointer);
    let byteOffset = pointer[0];
    let typedValue = this.fromByteOffset(declarator, byteOffset);
    return typedValue;
  }

  toDeclaredValue(declarator, value) {
    let result = this.constructor.coerce(declarator, value);
    if (declarator.pointer) {
      result = this.toPointer(declarator, result);
    }
    return result;
  }

  fromDeclaredValue(declarator, value) {
    let result;
    if (declarator.pointer) {
      result = this.fromPointer(declarator, value);
    }
    else {
      result = this.constructor.coerce(declarator, value);
    }
    return result;
  }

  acquireParameterValue(declarator, index) {
    const values = this.values;
    let value = values[index];
    let result = this.toDeclaredValue(declarator, value);
    return result;
  }

  acquireParameterValues() {
    const parameters = this.prototype.parameters;
    let result = parameters.map(this.acquireParameterValue, this);
    return result;
  }

  acquireReturnValue(value) {
    const prototype = this.prototype;
    let result = this.fromDeclaredValue(prototype, value);
    return result;
  }

  getParameterEntry(declarator, index) {
    if (!declarator) {
      return;
    }
    const parameterValues = this.parameterValues;
    const identifier = declarator.identifier;
    let parameterValue = parameterValues[index];
    let value = this.fromDeclaredValue(declarator, parameterValue);
    let result = [identifier, value];
    return result;
  }

  getOutputEntry(declarator, index) {
    if (!declarator) {
      return;
    }
    let parameterEntry = this.getParameterEntry(declarator, index);
    let identifier = parameterEntry[0];
    let value = parameterEntry[1].valueOf();
    let result = [identifier, value];
    return result;
  }

  getIndex(method, parameters) {
    method = method || this.getParameterEntry;
    parameters = parameters || this.prototype.parameters;
    let entries = parameters.map(method, this);
    entries = entries.filter(Boolean);
    let result = Object.fromEntries(entries);
    return result;
  }

  getParameterIndex(parameters) {
    const method = this.getParameterEntry;
    let result = this.getIndex(method, parameters);
    return result;
  }

  getOutputIndex(parameters) {
    const method = this.getOutputEntry;
    parameters = parameters || this.prototype.parameters;
    parameters = parameters.map(parameter => parameter.output && parameter);
    let result = this.getIndex(method, parameters);
    return result;
  }

  execute() {
    const options = this.options;
    const instance = this.instance;
    const targetFunction = this.targetFunction;
    let parameterValues = this.acquireParameterValues();
    this.parameterValues =
      this.onParameterValues(parameterValues) || parameterValues;
    let value = targetFunction.apply(instance, this.parameterValues);
    this.returnValue = this.acquireReturnValue(value);
    this.outputs = this.getOutputIndex();
    return this.returnValue;
  }
}
Invocation.WrongCardinalityError = WrongCardinalityError;

export default Invocation;

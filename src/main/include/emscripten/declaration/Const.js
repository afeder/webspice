class ConstantAssignmentError extends TypeError {
  constructor() {
    const message = "Assignment to constant.";
    super(message);
  }
}

class Const {
  static lock(target) {
    const originalSet = target.prototype.set;
    target.prototype.set = function (...args) {
      originalSet.apply(this, args);
      this.set = function () {
        throw new ConstantAssignmentError();
      }
    }
    return target;
  }

  constructor() {
    Const.lock(this);
  }

  set() {
  }
}
Const.ConstantAssignmentError = ConstantAssignmentError;

export { ConstantAssignmentError };
export default Const;

import ModuleShell from "./emscripten/ModuleShell.js";
import WebspiceContext from "./WebspiceContext.js";
import tkvrsn_c from "./spice/prototypes/tkvrsn_c.js";

const defaultIncludes = [tkvrsn_c];

class WebspiceShell extends ModuleShell {
  static async instantiate(includes, options) {
    let webspiceContext = await WebspiceContext.instantiate(options);
    let webspiceShell = new WebspiceShell(webspiceContext, includes);
    return webspiceShell;
  }

  constructor(moduleContext, includes) {
    super(moduleContext, includes);
    this.constructor.bind(this, this.constructor.defaultIncludes);
  }
}
WebspiceShell.defaultIncludes = defaultIncludes;

export default WebspiceShell;

import Type from "./Type.js";
import Delimiter from "./Delimiter.js";

const defaultInput = "";
const defaultDelimiter = "/"

class Path {
  constructor(input = Path.defaultInput, delimiter = Path.defaultDelimiter) {
    let sanitizedDelimiter = String(delimiter);
    this.delimiter = new Delimiter(sanitizedDelimiter);
    if (Type.isString(input)) {
      this.parts = this.delimiter.split(input);
    }
    else if (Type.isArray(input)) {
      this.parts = input;
    }
    else if (input instanceof Array) {
      this.parts = input;
    }
  }

  copy(input = this.parts, delimiter = this.delimiter) {
    let result = new Path(input, delimiter);
    return result;
  }

  get dir() {
    let resultParts = this.parts.slice(0, -1);
    let result = new Path(resultParts);
    return result;
  }

  get dirname() {
    let dir = this.dir;
    let result = String(dir);
    return result;
  }

  append(path) {
    let endPath = this.copy(path);
    let endParts = endPath.parts;
    let resultParts = this.parts.concat(endParts);
    let result = this.copy(resultParts);
    return result;
  }

  toString() {
    let parts = this.parts;
    let result = this.delimiter.join(parts);
    return result;
  }
}
Path.defaultInput = defaultInput;
Path.defaultDelimiter = defaultDelimiter;

export default Path;

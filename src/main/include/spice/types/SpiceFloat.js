import Float from "../../emscripten/types/Float.js";

// upstream/import/cspice/include/SpiceZdf.h
export class SpiceFloat extends Float {
}

export default SpiceFloat;

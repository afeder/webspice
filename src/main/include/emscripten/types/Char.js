import Type from "../../util/Type.js";

class InvalidCharacterError extends RangeError {
  constructor(character, index, characterArray) {
    let message = `Invalid character "${character}"`
    if (Type.isInteger(index)) {
      message += ` at index ${index}`;
    }
    super(message);
    this.character = character;
    this.index = index;
    this.characterArray = characterArray;
  }
}

class Char extends Uint8Array {
  static getCharCode(character, index, characterArray) {
    if (!character && index && characterArray) {
      character = characterArray[index];
    }
    if (!(character && character.charCodeAt)) {
      throw new InvalidCharacterError(character, index, characterArray);
    }
    let charCode = character.charCodeAt();
    if (charCode > 255) {
      throw new InvalidCharacterError(character, index, characterArray);
    }
    return charCode;
  }

  static getCharCodeArray(input, terminate = true) {
    const map = Array.prototype.map;
    let charCodeArray = map.call(input, Char.getCharCode);
    terminate && charCodeArray.push(Char.TERMINATOR);
    return charCodeArray;
  }

  static process(args) {
    if (Type.isString(args[0])) {
      let charCodeArray = Char.getCharCodeArray(args[0], true);
      args[0] = charCodeArray;
    }
    return args;
  }

  constructor(...args) {
    args = Char.process(args);
    super(...args);
  }

  set(...args) {
    args = Char.process(args);
    super.set(...args);
  }

  toString() {
    let terminatorIndex = this.indexOf(Char.TERMINATOR);
    let charCodeArray = this.slice(0, terminatorIndex);
    let result = String.fromCharCode(...charCodeArray);
    return result;
  }

  valueOf() {
    let result = this.toString();
    return result;
  }
}
Char.pointer = true;
Char.cardinality = null;
Char.TERMINATOR = 0;

export default Char;

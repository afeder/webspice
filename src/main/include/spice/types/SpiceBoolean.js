import Int from "../../emscripten/types/Int.js";

// upstream/import/cspice/include/SpiceZdf.h
export class SpiceBoolean extends Int {
}
SpiceBoolean.SPICETRUE = 1;
SpiceBoolean.SPICEFALSE = 0;

export default SpiceBoolean;

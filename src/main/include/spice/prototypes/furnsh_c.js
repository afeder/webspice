import FunctionDeclarator from "../../emscripten/declaration/FunctionDeclarator.js";
import ConstSpiceChar from "../types/ConstSpiceChar.js";

const furnsh_c = new FunctionDeclarator("furnsh_c", null);
furnsh_c.addParameter("file", ConstSpiceChar, true);

export {furnsh_c};
export default furnsh_c;

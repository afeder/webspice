import FunctionDeclarator from "../../emscripten/declaration/FunctionDeclarator.js";
import ConstSpiceChar from "../types/ConstSpiceChar.js";
import SpiceInt from "../types/SpiceInt.js";
import SpiceChar from "../types/SpiceChar.js";

const erract_c = new FunctionDeclarator("erract_c", null);
erract_c.addParameter("op", ConstSpiceChar, true);
erract_c.addParameter("lenout", SpiceInt);
erract_c.addParameter("action", SpiceChar, true, undefined, true);

export {erract_c};
export default erract_c;

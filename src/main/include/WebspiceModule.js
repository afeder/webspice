import Path from "./util/Path.js";
import Module from "./emscripten/Module.js";
import libwebspice from "./libwebspice.js";

const defaultOptions = Module.defaultOptions;

if (import.meta && import.meta.url) {
  let thisPath = new Path(import.meta.url, "/");
  let baseUrl = thisPath.dirname;
  defaultOptions["basePath"] = baseUrl;
}

class WebspiceModule extends Module {
  static instantiate(options = WebspiceModule.defaultOptions) {
    let inputModule = new WebspiceModule(options);
    let instantiateLibrary = libwebspice(inputModule);
    let instantiateWebspice = new Promise(function(resolve, reject) {
      instantiateLibrary.then(
        function (libraryInstance) {
          // Remove 'then' method to avoid infinite loop if called with 'await'.
          delete libraryInstance.then;
          resolve(libraryInstance);
        }
      );
    });
    return instantiateWebspice;
  }

  constructor(options = WebspiceModule.defaultOptions) {
    super(options);
  }
}
WebspiceModule.defaultOptions = defaultOptions;

export default WebspiceModule;

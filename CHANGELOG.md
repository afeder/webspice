## [0.0.2] - 2023-02-09
- Updated CSPICE to version 67.
- We now build the static library with a script derived from the upstream
  libcspice build script to minimize divergence from the upstream build. We also
  use customized versions of some CSPICE source files to resolve certain errors
  specific to our build.
- Added a build step to create a source code tarball.
- Made various minor corrections to the build process reflecting changes in
  Emscripten since the last release.
- Made minor improvements to the readability of the documentation.

## [0.0.1] - 2019-11-09
- Initial release.

[Unreleased]: https://gitlab.com/afeder/webspice/-/compare/0.0.2...develop
[0.0.2]: https://gitlab.com/afeder/webspice/-/tags/0.0.2
[0.0.1]: https://gitlab.com/afeder/webspice/-/tags/0.0.1

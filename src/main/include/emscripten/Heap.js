const initialStatistics = {
  "blocksUsed": 0,
  "bytesUsed": 0,
}

class Heap {
  static allocate(moduleContext, typedArray) {
    const instance = moduleContext.instance;
    const heap = instance.HEAPU8;
    const buffer = heap.buffer;
    const length = typedArray.length;
    const byteLength = typedArray.byteLength;
    let byteOffset = instance._malloc(byteLength);
    let allocation = new typedArray.constructor(buffer, byteOffset, length);
    allocation.set(typedArray);
    return allocation;
  }

  static free(moduleContext, allocation) {
    const instance = moduleContext.instance;
    const byteOffset = allocation.byteOffset;
    let result = instance._free(byteOffset);
    return result;
  }

  constructor(moduleContext) {
    this.moduleContext = moduleContext;
    this.statistics = initialStatistics;
  }

  get instance() {
    const moduleContext = this.moduleContext;
    const result = moduleContext.instance;
    return result;
  }

  get buffer() {
    const instance = this.instance;
    const heapArray = instance.HEAPU8;
    let result = heapArray.buffer;
    return result;
  }

  get byteLength() {
    const buffer = this.buffer;
    let result = buffer.byteLength;
    return result;
  }

  containsView(view) {
    let result = view.buffer === this.buffer;
    return result;
  }

  register(allocation) {
    const byteLength = allocation.byteLength;
    this.statistics.blocksUsed++;
    this.statistics.bytesUsed += byteLength;
  }

  deregister(allocation) {
    const byteLength = allocation.byteLength;
    this.statistics.blocksUsed--;
    this.statistics.bytesUsed -= byteLength;
  }

  allocate(typedArray) {
    const moduleContext = this.moduleContext;
    let allocation = this.constructor.allocate(moduleContext, typedArray);
    this.register(allocation);
    allocation.free = this.free.bind(this, allocation);
    return allocation;
  }

  free(allocation) {
    const moduleContext = this.moduleContext;
    let result = this.constructor.free(moduleContext, allocation);
    this.deregister(allocation);
    delete allocation.free;
    return result;
  }
}

export default Heap;

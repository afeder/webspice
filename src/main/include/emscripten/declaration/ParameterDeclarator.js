import Declarator from "./Declarator.js";

class ParameterDeclarator extends Declarator {
  constructor(identifier, type, pointer, cardinality, output = false) {
    super(identifier, type, pointer, cardinality);
    this.output = output;
  }
}

export default ParameterDeclarator;

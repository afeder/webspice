import Resource from "../web/Resource.js";

class File {
  static async makeData(input, onProgress) {
    let output;
    if (input instanceof Resource) {
      output = await input.fetch(onProgress);
    }
    else if (input instanceof ArrayBuffer) {
      output = new Uint8Array(input);
    }
    else {
      output = input;
    }
    return output;
  }

  constructor(fileSystem, path) {
    this.fileSystem = fileSystem;
    this.path = path;
  }

  readFile(opts) {
    let fileSystem = this.fileSystem;
    let path = this.path;
    let data = fileSystem.readFile(path, opts);
    return data;
  }

  writeFile(data, opts) {
    let fileSystem = this.fileSystem;
    let path = this.path;
    let result = fileSystem.writeFile(path, data, opts);
    return result;
  }
}

export default File;

import Int from "../../emscripten/types/Int.js";

// upstream/import/cspice/include/SpiceZdf.h
export class SpiceInt extends Int {
}

export default SpiceInt;

import FunctionDeclarator from "../../emscripten/declaration/FunctionDeclarator.js";
import ConstSpiceChar from "../types/ConstSpiceChar.js";

const tkvrsn_c = new FunctionDeclarator("tkvrsn_c", ConstSpiceChar);
tkvrsn_c.addParameter("item", ConstSpiceChar, true);

export {tkvrsn_c};
export default tkvrsn_c;

import SpiceDouble from "../spice/types/SpiceDouble.js";
import vnorm_c from "../spice/prototypes/vnorm_c.js";

function vnorm(v) {
  const webspice = this.webspice;
  const functions = webspice.functions;
  const heap = webspice.heap;
  webspice.include(vnorm_c);
  let v1 = new SpiceDouble(v);
  let result = functions.vnorm_c(v1);
  result = result[0];
  return result;
}

export {vnorm};
export default vnorm;

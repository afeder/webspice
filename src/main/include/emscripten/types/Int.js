class Int extends Int32Array {
  valueOf() {
    let result = Array.from(this);
    return result;
  }
}
Int.pointer = false;
Int.cardinality = 1;

export default Int;

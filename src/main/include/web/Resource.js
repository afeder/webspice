class InvalidResponseError extends Error {
  constructor(response) {
    const status = response.status;
    const statusText = response.statusText;
    let message = `Invalid response: ${status} ${statusText}.`;
    super(message);
    this.response = response;
  }
}

class Resource {
  static async readResponse(response, onProgress) {
    const headerContentLength = response.headers.get("Content-Length");
    let reader = response.body.getReader();
    let values = [];
    let contentLength = Number(headerContentLength);
    let receivedLength = 0;
    let done = false;
    while (!done) {
      let chunk = await reader.read();
      if (chunk.value !== undefined) {
        values.push(chunk.value);
        receivedLength += chunk.value.length;
      }
      done = chunk.done;
      let progress = {contentLength, receivedLength, done};
      onProgress && onProgress.call(response, progress);
    }

    let data = new Uint8Array(receivedLength);
    let memorizedLength = 0;
    for (let value of values) {
      data.set(value, memorizedLength);
      memorizedLength += value.length;
    }
    return data;
  }

  constructor(source, init) {
    this.source = source;
    this.init = init;
  }

  async fetch(onProgress, init = this.init) {
    const source = this.source;
    let response = await fetch(source, init);
    if (!response.ok) {
      throw new InvalidResponseError(response);
    }
    let data = await this.constructor.readResponse(response, onProgress);
    return data;
  }
}
Resource.InvalidResponseError = InvalidResponseError;

export default Resource;

import SpiceFloat from "./SpiceFloat.js";
import Const from "../../emscripten/declaration/Const.js";

// upstream/import/cspice/include/SpiceZdf.h
export class ConstSpiceFloat extends SpiceFloat {
}
Const.lock(ConstSpiceFloat)

export default ConstSpiceFloat;

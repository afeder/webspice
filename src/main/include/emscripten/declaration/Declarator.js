function ifUndefined(value, then, otherwise = value) {
  let result = value === undefined ? then : otherwise;
  return result;
}

function getDefaultPointer(type) {
  let result = Boolean(type && type.pointer);
  return result;
}

function getDefaultCardinality(type) {
  let result = type ? type.cardinality : null;
  return result;
}

class Declarator {
  constructor(identifier, type, pointer, cardinality) {
    this.identifier = identifier;
    this.type = type;
    this.pointer = ifUndefined(pointer, getDefaultPointer(type));
    this.cardinality = ifUndefined(cardinality, getDefaultCardinality(type));
  }
}

export default Declarator;
